package com.example.vasic.kvalifikacijeexecom.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


import com.example.vasic.kvalifikacijeexecom.R;
import com.example.vasic.kvalifikacijeexecom.model.Task;
import com.example.vasic.kvalifikacijeexecom.preference.taskPref_;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by vasic on 12/2/2016.
 */


@EActivity(R.layout.activity_update)
public class UpdateActivity extends AppCompatActivity {

    @ViewById
    EditText titleUpdate;

    @ViewById
    EditText descriptionUpdate;

    @ViewById
    CheckBox isChecked;

    @ViewById
    Button saveTask;

    @Pref
    taskPref_ taskPref;
    private Task task;
    @AfterViews
    void setTitleUpdate() {
        if(taskPref.task().exists()){ boolean ima=true;}
        final Gson gson = new Gson();
          task = gson.fromJson(taskPref.task().get(), Task.class);
        titleUpdate.setText(task.getTitle().toString() );
        descriptionUpdate.setText(task.getDescription().toString());
        isChecked.setChecked(task.isFinished());

    }

    @Click
    void saveTask(){
        final Gson gson = new Gson();
        final Task taskUpdated = new Task(task.getId(),titleUpdate.getText().toString(),
                descriptionUpdate.getText().toString(),isChecked.isChecked());
        final Intent intent = new Intent();

        intent.putExtra("task", gson.toJson(taskUpdated));
        setResult(RESULT_OK, intent);
        finish();
    }
}
