package com.example.vasic.kvalifikacijeexecom.preference;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by vasic on 12/4/2016.
 */
@SharedPref(SharedPref.Scope.UNIQUE)
public interface taskPref {
    String task();
}
